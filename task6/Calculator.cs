﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task6
{
    internal class Calculator
    {
        public dynamic a {  get; set; }
        public dynamic b {  get; set; }

        public Calculator(dynamic a, dynamic b)
        {
            this.a = a;
            this.b = b;
        }

        public dynamic Add()
        {
            return a + b;
        }

        public dynamic Sub()
        {
            return a - b;
        }

        public dynamic Mul()
        {
            return a * b;
        }

        public dynamic Div()
        {
            
            return b == 0? 0 : a / b;
        }
    }
}
