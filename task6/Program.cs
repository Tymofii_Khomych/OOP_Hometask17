﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task6
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Calculator calc = new Calculator("aaa", 10);

            Console.WriteLine($"{calc.a} + {calc.b} = {calc.Add()}");
            Console.WriteLine($"{calc.a} - {calc.b} = {calc.Sub()}");
            Console.WriteLine($"{calc.a} * {calc.b} = {calc.Mul()}");
            Console.WriteLine($"{calc.a} / {calc.b} = {calc.Div()}");
        }
    }
}
