﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using task2;

namespace task2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            var autoCollection = new List<Auto>
            {
                new Auto("Toyota", "Corolla", 2019, "Red"),
                new Auto("Honda", "Civic", 2020, "Blue"),
                new Auto("Ford", "Mustang", 2021, "Black"),
                new Auto("Chevrolet", "Cruze", 2018, "White"),
                new Auto("BMW", "X5", 2022, "Silver"),
                new Auto("Audi", "A4", 2017, "Gray"),
                new Auto("Mercedes-Benz", "E-Class", 2023, "Green"),
                new Auto("Volkswagen", "Golf", 2020, "Yellow"),
                new Auto("Hyundai", "Elantra", 2019, "Orange"),
                new Auto("Kia", "Sportage", 2021, "Purple")
            };

            var clientCollection = new List<Client>
            {
                new Client("Camry","John Smith","+1234567890"),
                new Client("Civic", "Emma Johnson", "+9876543210"),
                new Client("Mustang", "Michael Williams", "+5555555555"),
                new Client("Volkswagen", "Sophia Brown", "+1111111111"),
                new Client("X5","Daniel Davis","+2222222222"),
                new Client("Kia","Olivia Taylor","+3333333333"),
                new Client("Ford", "James Anderson", "+4444444444"),
                new Client("Kia", "Isabella Martinez", "+6666666666"),
                new Client("Sportage", "William Johnson", "+7777777777"),
                new Client("Ford", "Ava Wilson", "+9999999999")
            };

            var query = from client in clientCollection
                        join car in autoCollection
                        on client.CarModel equals car.CarModel
                        orderby client.Name ascending
                        select new
                        {
                            Name = client.Name,
                            PhoneNumber = client.PhoneNumber,
                            Brand = car.Brand,
                            CarModel = car.CarModel,
                            Year = car.ManufectureYear,
                            Color = car.Color
                        };

            foreach(var item in query)
            {
                Console.WriteLine($"Client name: {item.Name}");
                Console.WriteLine($"Client phone number: {item.PhoneNumber}");
                Console.WriteLine($"Car brand: {item.Brand}");
                Console.WriteLine($"Car model: {item.CarModel}");
                Console.WriteLine($"Car manufacture year: {item.Year}");
                Console.WriteLine($"Car color: {item.Color}");
                Console.WriteLine();
            }
        }
    }
}
