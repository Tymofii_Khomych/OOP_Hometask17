﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.Unicode;
            var dictionary = new List<dynamic>
            {
                new { EnglishWord = "apple", UkrainianTranslation = "яблуко" },
                new { EnglishWord = "cat", UkrainianTranslation = "кіт" },
                new { EnglishWord = "book", UkrainianTranslation = "книга" },
                new { EnglishWord = "dog", UkrainianTranslation = "собака" },
                new { EnglishWord = "house", UkrainianTranslation = "будинок" },
                new { EnglishWord = "car", UkrainianTranslation = "автомобіль" },
                new { EnglishWord = "tree", UkrainianTranslation = "дерево" },
                new { EnglishWord = "sun", UkrainianTranslation = "сонце" },
                new { EnglishWord = "computer", UkrainianTranslation = "комп'ютер" },
                new { EnglishWord = "flower", UkrainianTranslation = "квітка" }
            };

            Console.WriteLine("Англо-Російський словник:");
            foreach (var item in dictionary)
            {
                Console.WriteLine($"{item.EnglishWord} - {item.UkrainianTranslation}");
            }
        }
    }
}
