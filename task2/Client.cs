﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Client
    {
        public string CarModel { set; get; }
        public string Name { set; get; }
        public string PhoneNumber { set; get; }

        public Client(string carModel, string name, string phoneNumber)
        {
            CarModel = carModel;
            Name = name;
            PhoneNumber = phoneNumber;
        }
    }
}
