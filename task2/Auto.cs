﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task2
{
    internal class Auto
    {
        public string Brand { set; get; }
        public string CarModel { set; get; }
        public int ManufectureYear { set; get; }
        public string Color { set; get; }

        public Auto(string brand, string carModel, int manufectureYear, string color)
        {
            Brand = brand;
            CarModel = carModel;
            ManufectureYear = manufectureYear;
            Color = color;
        }
    }
}
